module.exports = {
  transformIgnorePatterns: [
    "/node_modules/(?!(axios)/)",
    "^.+\\.module\\.(css|sass|scss)$",
  ],
  transform: {
    '^.+\\.(js|jsx|ts|tsx)$': 'babel-jest',
    '^.+\\.css$': 'jest-transform-stub', // Transform CSS files
    'node_modules/axios/.+\\.js$': 'babel-jest'
  },
  testEnvironment: 'jsdom',
  coveragePathIgnorePatterns: [
    "/node_modules/",
    "index.js",
  ],
  setupFiles: ['./jest.setup.js']
};