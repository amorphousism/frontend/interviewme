import React from 'react';
import { AskInput } from './AskInput';

function AskArea({ query, setQuery, onSearch }) {
  return (
      <div>
        <AskInput query={query} setQuery={setQuery} onSearch={onSearch} />
      </div>
  );
}

export default AskArea;