import './AskInput.css';

export const AskInput = ({ query, setQuery, onSearch }) => (
    <div className="ask-container">
      <input
          type="text"
          value={query}
          onChange={e => setQuery(e.target.value)}
          onKeyDown={e => e.key === 'Enter' && onSearch()}
          placeholder="Enter your query here..."
      />
      <button onClick={onSearch}>Ask</button>
    </div>
);