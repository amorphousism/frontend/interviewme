import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { AskInput } from './AskInput';
import '@testing-library/jest-dom';

describe('AskInput', () => {
  it('should render input element', () => {
    render(<AskInput query="" setQuery={() => {}} onSearch={() => {}} />);
    expect(screen.getByPlaceholderText('Enter your query here...')).toBeInTheDocument();
  });

  it('should handle input changes', () => {
    const setQuery = jest.fn();
    render(<AskInput query="" setQuery={setQuery} onSearch={() => {}} />);
    const input = screen.getByPlaceholderText('Enter your query here...');
    fireEvent.change(input, { target: { value: 'new query' } });
    expect(setQuery).toHaveBeenCalledWith('new query');
  });

  it('should call onSearch when Enter is pressed', () => {
    const onSearch = jest.fn();
    render(<AskInput query="test" setQuery={() => {}} onSearch={onSearch} />);
    const input = screen.getByPlaceholderText('Enter your query here...');
    fireEvent.keyDown(input, { key: 'Enter', code: 'Enter' });
    expect(onSearch).toHaveBeenCalled();
  });
});