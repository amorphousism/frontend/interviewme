import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import AskArea from './AskArea';
import '@testing-library/jest-dom';

describe('SearchArea Component', () => {
  it('calls onSearch when the search button is clicked', () => {
    const mockOnSearch = jest.fn();
    render(<AskArea query="" setQuery={() => {}} onSearch={mockOnSearch} />);
    fireEvent.change(screen.getByRole('textbox'), { target: { value: 'React' } });
    fireEvent.click(screen.getByText('Ask'));
    expect(mockOnSearch).toHaveBeenCalled();
  });
});