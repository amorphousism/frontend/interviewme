import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';
import FeedbackModal from './FeedbackModal';  // 调整相对路径以匹配你的项目结构

describe('FeedbackModal', () => {
  test('should render correctly when isOpen is true', () => {
    render(<FeedbackModal isOpen={true} onClose={() => {}} onSubmit={() => {}} />);
    expect(screen.getByText('Feedback Form')).toBeInTheDocument();
    expect(screen.getByLabelText('Name:')).toBeInTheDocument();
    expect(screen.getByLabelText('Company:')).toBeInTheDocument();
    expect(screen.getByLabelText('Email:')).toBeInTheDocument();
    expect(screen.getByLabelText('Feedback:')).toBeInTheDocument();
  });

  test('should not render when isOpen is false', () => {
    render(<FeedbackModal isOpen={false} onClose={() => {}} onSubmit={() => {}} />);
    expect(screen.queryByText('Feedback Form')).not.toBeInTheDocument();
  });

  test('should call onClose when close button is clicked', () => {
    const handleClose = jest.fn();
    render(<FeedbackModal isOpen={true} onClose={handleClose} onSubmit={() => {}} />);
    const closeButton = screen.getByText('Close');
    userEvent.click(closeButton);
    expect(handleClose).toHaveBeenCalledTimes(1);
  });

  test('should call onSubmit with the feedback data when form is submitted', () => {
    const handleSubmit = jest.fn();
    render(<FeedbackModal isOpen={true} onClose={() => {}} onSubmit={handleSubmit} />);
    userEvent.type(screen.getByLabelText('Name:'), 'John Doe');
    userEvent.type(screen.getByLabelText('Company:'), 'Acme Inc');
    userEvent.type(screen.getByLabelText('Email:'), 'john@example.com');
    userEvent.type(screen.getByLabelText('Feedback:'), 'Great service!');

    const submitButton = screen.getByText('Submit');
    userEvent.click(submitButton);

    expect(handleSubmit).toHaveBeenCalledWith({
      name: 'John Doe',
      company: 'Acme Inc',
      email: 'john@example.com',
      feedback: 'Great service!'
    });
  });
});