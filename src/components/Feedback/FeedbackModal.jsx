import React, { useState } from 'react';
import './FeedbackModal.css';

function FeedbackModal({ isOpen, onClose, onSubmit }) {
  const [name, setName] = useState('');
  const [company, setCompany] = useState('');
  const [email, setEmail] = useState('');
  const [feedback, setFeedback] = useState('');

  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/; // 简单的电子邮件正则表达式

  const handleSubmit = (event) => {
    event.preventDefault();
    if (!name || !company || !emailRegex.test(email)) {
      alert('Please enter valid name, company, and email.');
      return;
    }
    onSubmit({ name, company, email, feedback });
    onClose();
  };

  if (!isOpen) {
    return null;
  }

  return (
      <div className="modal-background">
        <div className="modal-content">
          <div className="modal-header">
            <h3>Feedback Form</h3>
          </div>
          <form onSubmit={handleSubmit}>
            <div className="modal-body">
              <label htmlFor="name" className="modal-label">Name:</label>
              <input id="name" type="text" className="modal-input" value={name} onChange={(e) => setName(e.target.value)} />

              <label htmlFor="company" className="modal-label">Company:</label>
              <input id="company" type="text" className="modal-input" value={company} onChange={(e) => setCompany(e.target.value)} />

              <label htmlFor="email" className="modal-label">Email:</label>
              <input id="email" type="email" className="modal-input" value={email} onChange={(e) => setEmail(e.target.value)} />

              <label htmlFor="feedback" className="modal-label">Feedback:</label>
              <textarea id="feedback" className="modal-textarea" value={feedback} onChange={(e) => setFeedback(e.target.value)} />
            </div>
            <div className="modal-footer">
              <button type="button" className="modal-close-btn" onClick={onClose}>Close</button>
              <button type="submit" className="modal-submit-btn">Submit</button>
            </div>
          </form>
        </div>
      </div>
  );
}

export default FeedbackModal;