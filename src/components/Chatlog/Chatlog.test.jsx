import React from 'react';
import { render, screen } from '@testing-library/react';
import { Chatlog } from './Chatlog';
import '@testing-library/jest-dom';

// Mock the ChatlogItem component to simplify the test.
jest.mock('./ChatlogItem', () => (props) => <div data-testid="chatlog-item">{props.item.query}</div>);

describe('Chatlog', () => {
  it('renders correctly with multiple items', () => {
    const chatlog = [
      { query: 'First query', response: 'First response' },
      { query: 'Second query', response: 'Second response' }
    ];
    render(<Chatlog log={chatlog} />);
    const items = screen.getAllByTestId('chatlog-item');
    expect(items.length).toBe(2);
    // Check if the items are rendered in reverse order
    expect(items[0].textContent).toBe('Second query');
    expect(items[1].textContent).toBe('First query');
  });

  it('renders correctly with an empty chatlog', () => {
    render(<Chatlog log={[]} />);
    const items = screen.queryByTestId('chatlog-item');
    expect(items).toBeNull();
  });
});