export const ChatlogDownloadButton = ({ chatlog }) => {
  const handleDownload = () => {
    const dataStr = JSON.stringify(chatlog);
    const blob = new Blob([dataStr], { type: "application/json" });
    const url = URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.download = "interview-data.json";
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    URL.revokeObjectURL(url);
  };

  return (
      <button onClick={handleDownload} disabled={!chatlog.length}>
        Download Interview
      </button>
  );
};