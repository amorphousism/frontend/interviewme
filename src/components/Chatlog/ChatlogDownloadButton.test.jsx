import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import {ChatlogDownloadButton} from './ChatlogDownloadButton';
import '@testing-library/jest-dom';

describe('ChatlogDownloadButton', () => {
  // 模拟全局对象和方法
  beforeAll(() => {
    global.URL.createObjectURL = jest.fn();
    global.URL.revokeObjectURL = jest.fn();
  });

  afterAll(() => {
    global.URL.createObjectURL.mockRestore();
    global.URL.revokeObjectURL.mockRestore();
  });

  it('should download chat log when the button is clicked', () => {
    const chatlog = [{ query: 'What is React?', response: 'A JS library' }];
    render(<ChatlogDownloadButton chatlog={chatlog} />);
    const button = screen.getByRole('button', { name: 'Download Interview' });
    fireEvent.click(button);
    expect(global.URL.createObjectURL).toHaveBeenCalled();
  });

  it('should have disabled button when chat log is empty', () => {
    render(<ChatlogDownloadButton chatlog={[]} />);
    const button = screen.getByRole('button', { name: 'Download Interview' });
    expect(button).toBeDisabled();
  });
});