import React from 'react';
import ChatlogItem from './ChatlogItem';
import './Chatlog.css';

export const Chatlog = ({ log }) => (
    <div className="chatlog">
        {[...log].reverse().map((item, index) => (
            <ChatlogItem key={index} item={item} />
        ))}
    </div>
);