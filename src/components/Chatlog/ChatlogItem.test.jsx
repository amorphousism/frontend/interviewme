import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import ChatlogItem from './ChatlogItem';
import '@testing-library/jest-dom';

describe('ChatlogItem', () => {
  it('should display query and response', () => {
    const item = {
      query: 'What is React?',
      response: 'A JavaScript library for building user interfaces.',
      sources: []
    };
    render(<ChatlogItem item={item} />);
    expect(screen.getByText(/Query:/i)).toBeInTheDocument();
    expect(screen.getByText('What is React?')).toBeInTheDocument();
    expect(screen.getByText(/Response:/i)).toBeInTheDocument();
    expect(screen.getByText('A JavaScript library for building user interfaces.')).toBeInTheDocument();
  });

  it('should toggle sources visibility', () => {
    const item = {
      query: 'What is React?',
      response: 'A JavaScript library for building user interfaces.',
      sources: [{ source: 'https://reactjs.org', page_content: 'Official React Documentation' }]
    };
    render(<ChatlogItem item={item} />);
    const button = screen.getByText('Show Sources');
    fireEvent.click(button);
    // 使用包含文本的查询来应对可能的空格和换行问题
    expect(screen.getByText(/Source URL:/i)).toBeInTheDocument();
    expect(screen.getByText('https://reactjs.org')).toBeInTheDocument();
    fireEvent.click(button);
    // 检查元素是否不可见或已从 DOM 中移除
    expect(screen.queryByText(/Source URL:/i)).toBeNull();
    expect(screen.queryByText('https://reactjs.org')).toBeNull();
  });
});