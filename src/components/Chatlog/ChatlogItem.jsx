import React, { useState } from 'react';
import './ChatlogItem.css';

const ChatlogItem = ({ item }) => {
  const [isExpanded, setIsExpanded] = useState(false);

  return (
      <div className="chatlog-item">
        <p><strong>Query:</strong> {item.query}</p>
        <p><strong>Response:</strong> {item.response}</p>
        <button onClick={() => setIsExpanded(!isExpanded)}>
          {isExpanded ? 'Hide Sources' : 'Show Sources'}
        </button>
        {isExpanded && item.sources && item.sources.length > 0 && (
            <div>
              <strong>Sources:</strong>
              <ul>
                {item.sources.map((source, idx) => (
                    <li key={idx}>
                      <strong>Source URL:</strong> {source.source}
                      <p><strong>Content:</strong> {source.page_content}</p>
                    </li>
                ))}
              </ul>
            </div>
        )}
      </div>
  );
};

export default ChatlogItem;