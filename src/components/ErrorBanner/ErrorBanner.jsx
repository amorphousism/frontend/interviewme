import React from 'react';
import './ErrorBanner.css';

function ErrorBanner({ message, onClose }) {
  if (!message) return null;

  return (
      <div className="error-banner">
        {message}
        <span className="close-btn" onClick={onClose}>❌</span>
      </div>
  );
}

export default ErrorBanner;