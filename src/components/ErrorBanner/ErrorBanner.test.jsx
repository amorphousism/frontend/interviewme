import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import ErrorBanner from "./ErrorBanner";
import '@testing-library/jest-dom';

describe("ErrorBanner", () => {
  it("should not render if 'message' is empty", () => {
    render(<ErrorBanner message="" onClose={() => {}} />);
    const banner = screen.queryByText(/❌/i);
    expect(banner).toBeNull();
  });

  it("should display the message", () => {
    render(<ErrorBanner message="Test error message" onClose={() => {}} />);
    expect(screen.getByText("Test error message")).toBeInTheDocument();
  });

  it("should call onClose when close button is clicked", () => {
    const onClose = jest.fn();
    render(<ErrorBanner message="Test error message" onClose={onClose} />);
    fireEvent.click(screen.getByText(/❌/i));
    expect(onClose).toHaveBeenCalledTimes(1);
  });
});