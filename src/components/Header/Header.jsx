import React from 'react';
import { Tooltip } from 'react-tooltip';

function Header() {
  return (
      <div>
        <Tooltip
            id="header-tooltip"
            place="top"
            variant="info"
            content="This webapp is powered by Llama3🦙 & 🦜LangChain on top of some of my mock or real interviews"/>
        <h1 data-tooltip-id="header-tooltip">Interview me</h1>
      </div>
  );
}

export default Header;