import React from 'react';
import {render, screen} from '@testing-library/react';
import Header from './Header';
import '@testing-library/jest-dom';

describe('Header Component', () => {
  it('renders the header with the correct text', () => {
    render(<Header />);
    expect(screen.getByText('Interview me')).toBeInTheDocument();
  });
});