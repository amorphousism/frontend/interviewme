import React from 'react';
import { render, screen } from '@testing-library/react';
import MainContent from './MainContent';
import '@testing-library/jest-dom';

describe('MainContent Component', () => {
  it('renders chatlog and download button', () => {
    const chatlog = [{ query: 'What is React?', response: 'A JS library', sources: ['https://reactjs.org'] }];
    render(<MainContent chatlog={chatlog} onDownload={() => {}} />);
    expect(screen.getByText('A JS library')).toBeInTheDocument();
    expect(screen.getByText('Download Interview')).toBeInTheDocument();
  });
});