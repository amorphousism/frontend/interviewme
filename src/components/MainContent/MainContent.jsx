import React from 'react';
import { Chatlog } from '../Chatlog/Chatlog';
import { ChatlogDownloadButton } from '../Chatlog/ChatlogDownloadButton';

function MainContent({ chatlog, onDownload }) {
  return (
      <div>
        <Chatlog log={chatlog} />
        <ChatlogDownloadButton chatlog={chatlog} onDownload={onDownload} />
      </div>
  );
}

export default MainContent;