// src/App.test.jsx
import React from 'react';
import { render, fireEvent, screen, waitFor } from '@testing-library/react';
import { ask } from './api/interviewQueryApi';
import App from './App';
import {sendFeedback} from "./api/feedbackApi";
import '@testing-library/jest-dom';

jest.mock('./api/interviewQueryApi', () => ({
  ask: jest.fn()
}));

jest.mock('./api/feedbackApi', () => ({
  sendFeedback: jest.fn()
}));

describe('App Component', () => {
  beforeEach(() => {
    ask.mockClear();
    sendFeedback.mockClear();
  });

  it('handles successful search', async () => {
    ask.mockResolvedValue({
      answer: 'React is a JavaScript library for building user interfaces.',
      sources: ['https://reactjs.org']
    });

    render(<App />);
    fireEvent.change(screen.getByRole('textbox'), { target: { value: 'What is React?' } });
    fireEvent.click(screen.getByText('Ask'));

    await waitFor(() => {
      expect(screen.getByText('React is a JavaScript library for building user interfaces.')).toBeInTheDocument();
    });
  });

  it('displays error message on search failure', async () => {
    ask.mockRejectedValue(new Error('Network error'));

    render(<App />);
    fireEvent.change(screen.getByRole('textbox'), { target: { value: 'What is React?' } });
    fireEvent.click(screen.getByText('Ask'));

    await waitFor(() => {
      expect(screen.getByText('Failed to fetch data')).toBeInTheDocument();
    });
  });

  it('handles empty search query', async () => {
    render(<App />);
    fireEvent.change(screen.getByRole('textbox'), { target: { value: '' } });
    fireEvent.click(screen.getByText('Ask'));

    await waitFor(() => {
      expect(screen.getByText('Please enter a search query.')).toBeInTheDocument();
    });
  });

  it('closes the error banner when close button is clicked', async () => {
    render(<App />);
    fireEvent.change(screen.getByRole('textbox'), { target: { value: '' } });
    fireEvent.click(screen.getByText('Ask'));
    fireEvent.click(screen.getByText('❌'));
    await waitFor(() => {
      expect(screen.queryByText('Failed to fetch data')).not.toBeInTheDocument();
    });
  });

  it('shows loading indicator when fetching data', async () => {
    ask.mockImplementation(() => new Promise(resolve => setTimeout(() => resolve({
      answer: 'React is awesome!',
      sources: ['https://reactjs.org']
    }), 500)));

    render(<App />);
    fireEvent.change(screen.getByRole('textbox'), { target: { value: 'Tell me about React' } });
    fireEvent.click(screen.getByText('Ask'));

    expect(screen.getByText('Loading...')).toBeInTheDocument();
    await waitFor(() => {
      expect(screen.queryByText('Loading...')).not.toBeInTheDocument();
    });
  });
});