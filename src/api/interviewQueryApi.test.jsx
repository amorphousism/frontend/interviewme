import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { ask } from './interviewQueryApi';
import '@testing-library/jest-dom';
import {API_ENDPOINTS} from "./apiConfig";

describe('ask', () => {

  let mock;

  // 在每个测试前设置
  beforeEach(() => {
    mock = new MockAdapter(axios);
  });

  // 在每个测试后清理
  afterEach(() => {
    mock.reset();
  });

  it('should return data when searchQuery is called successfully', async () => {
    const fakeData = {
      answer: 'React is a JavaScript library for building user interfaces.',
      sources: ['https://reactjs.org']
    };
    mock.onPost(API_ENDPOINTS.ASK).reply(200, fakeData);

    const data = await ask('What is React?');
    expect(data).toEqual(fakeData);
  });

  it('should throw an error when searchQuery is called with a server error', async () => {
    mock.onPost(API_ENDPOINTS.ASK).reply(500);

    // 期望这个异步函数抛出错误
    await expect(ask('test query')).rejects.toThrow();
  });

  it('should handle network or other errors', async () => {
    mock.onPost(API_ENDPOINTS.ASK).networkError();

    await expect(ask('test query')).rejects.toThrow();
  });
});