import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { sendFeedback } from './feedbackApi';
import '@testing-library/jest-dom';
import {API_ENDPOINTS} from "./apiConfig";

describe('sendFeedback', () => {

  let mock;

  // 在每个测试前设置
  beforeEach(() => {
    mock = new MockAdapter(axios);
  });

  // 在每个测试后清理
  afterEach(() => {
    mock.reset();
  });

  it('should return data when sendFeedback is called successfully', async () => {
    const fakeFeedback = {
      name: 'John Doe',
      company: 'Example Inc.',
      contact: 'john.doe@example.com',
      feedback: 'Great service!'
    };
    const fakeResponse = { status: 'success', message: 'Feedback received' };
    mock.onPost(API_ENDPOINTS.FEEDBACK).reply(200, fakeResponse);

    const response = await sendFeedback(fakeFeedback);
    expect(response).toEqual(fakeResponse);
  });

  it('should throw an error when sendFeedback is called with a server error', async () => {
    const fakeFeedback = {
      name: 'Jane Doe',
      company: 'Test Corp',
      contact: 'jane.doe@test.com',
      feedback: 'Needs improvement.'
    };
    mock.onPost(API_ENDPOINTS.FEEDBACK).reply(500);

    // 期望这个异步函数抛出错误
    await expect(sendFeedback(fakeFeedback)).rejects.toThrow();
  });

  it('should handle network or other errors', async () => {
    const fakeFeedback = {
      name: 'Alice Smith',
      company: 'Innovate LLC',
      contact: 'alice.smith@innovate.com',
      feedback: 'Excellent!'
    };
    mock.onPost(API_ENDPOINTS.FEEDBACK).networkError();

    await expect(sendFeedback(fakeFeedback)).rejects.toThrow();
  });
});