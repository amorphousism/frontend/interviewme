const API_BASE_URL = process.env.REACT_APP_BACKEND_URL;
const API_VERSION = 'v1'; // 示例版本号

export const API_ENDPOINTS = {
  ASK: `${API_BASE_URL}/api/${API_VERSION}/ask`,
  FEEDBACK: `${API_BASE_URL}/api/${API_VERSION}/feedback`
};