import axios from 'axios';
import {API_ENDPOINTS} from "./apiConfig";

/**
 * 发送反馈数据到后端服务器。
 * @param {Object} feedback 包含反馈信息的对象。
 */
export const sendFeedback = async (feedback) => {
  try {
    const response = await axios.post(API_ENDPOINTS.FEEDBACK, feedback);
    return response.data;
  } catch (error) {
    console.error('Error submitting feedback:', error);
    throw error; // 重新抛出错误以便于在组件中处理
  }
};