import axios from 'axios';
import {API_ENDPOINTS} from "./apiConfig";

export const ask = async (query) => {
  try {
    const response = await axios.post(API_ENDPOINTS.ASK, { query });
    return response.data;
  } catch (error) {
    console.error('Error:', error);
    throw error; // 重新抛出错误以便于在组件中处理
  }
};