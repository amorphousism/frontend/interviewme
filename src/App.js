// App.js
import React, { useState, useCallback } from 'react';
import Header from './components/Header/Header';
import AskArea from './components/Ask/AskArea';
import MainContent from './components/MainContent/MainContent';
import FeedbackModal from './components/Feedback/FeedbackModal';
import './App.css';
import ErrorBanner from "./components/ErrorBanner/ErrorBanner";
import {ask} from "./api/interviewQueryApi";
import {sendFeedback} from "./api/feedbackApi";

function App() {
  const [query, setQuery] = useState('');
  const [chatlog, setChatlog] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState('');
  const [isFeedbackModalOpen, setFeedbackModalOpen] = useState(false);

  const handleCloseError = () => {
    setError('');
  };

  const handleAsk = useCallback(async () => {
    if (!query.trim()) {
      setError('Please enter a search query.');
      return;
    }
    setIsLoading(true);
    setError('');
    try {
      const data = await ask(query);
      const newEntry = {
        query,
        response: data.answer,
        sources: data.sources
      };
      setChatlog([...chatlog, newEntry]);
    } catch (error) {
      setError('Failed to fetch data');
      const newEntry = {query, response: 'connection failure', sources: []};
      setChatlog([...chatlog, newEntry]);
    } finally {
      setIsLoading(false);
      setQuery('');
    }
  }, [query, chatlog]);

  const handleFeedbackSubmit = async (feedback) => {
    console.log('Feedback received:', feedback);
    try {
      const response = await sendFeedback(feedback);
      console.log('Feedback submitted successfully:', response);
    } catch (error) {
      console.error('Error submitting feedback:', error);
      setError('Failed to send feedback');
    }
  };

  return (
      <div className="App">
        <ErrorBanner message={error} onClose={handleCloseError} />
        <Header />
        <button onClick={() => setFeedbackModalOpen(true)}>Leave your contact</button>
        <FeedbackModal
            isOpen={isFeedbackModalOpen}
            onClose={() => setFeedbackModalOpen(false)}
            onSubmit={handleFeedbackSubmit}
        />
        <AskArea query={query} setQuery={setQuery} onSearch={handleAsk} />
        {isLoading && <p>Loading...</p>}
        <MainContent chatlog={chatlog} onDownload={() => setFeedbackModalOpen(true)} />
      </div>
  );
}

export default App;